/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [
      {
        resolve: `gatsby-source-mongodb`,
        options: {
        connectionString: process.env.MONGODBURI,
        dbName: process.env.MONGODBNAME,
        collection: [`courses`,`instructors`],
        extraParams: {
            retryWrites: true,
            w: "majority",
        },
    }, 
    

    },
    
  ],
}
