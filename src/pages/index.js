import React from "react"
import { graphql } from "gatsby";

export default function Home({ data }) {
  return (
    <div>
      <h1>Full Stack Citation Courses</h1>
      <table>
        <thead>
            <tr>
                <th>Subject</th>
                <th>Course</th>
                <th>Title</th>
            </tr>
        </thead>
        <tbody>
            {data.allMongodbCoursesCourses.edges.map(({ node }, index) => (
                <tr key={index}>
                    <td>{node.subject}</td>
                    <td>{node.course}</td>
                    <td><a href={'/course/' + node.subject + '/' + node.course}>{node.title}</a></td>
                </tr>
            ))}
        </tbody>
      </table>
      <p><a href="/instructors">Instructors</a></p>
    </div>
  )
}

export const query = graphql`
query MyQuery {
  allMongodbCoursesCourses {
    edges {
      node {
        course
        subject
        title
      }
    }
  }
}
`