import React from "react"
import { graphql } from "gatsby";

export default function Home({ data }) {
  return (
    <div>
      <h1>Computer Science Dapartment Instructors</h1>
      <table>
        <thead>
            <tr>
                <th>Name:</th>
                <th>Phone:</th>
                <th>Office:</th>
                <th>Email:</th>
            </tr>
        </thead>
        <tbody>
            {data.allMongodbCoursesInstructors.edges.map(({ node }, index) => (
                <tr key={index}>
                    <td>{node.name}</td>
                    <td>{node.phone}</td>
                    <td>{node.office}</td>
                    <td>{node.email}</td>
                </tr>
            ))}
        </tbody>
      </table>
      <p><a href="/">Courses</a></p>
    </div>
  )
}

export const query = graphql`
query My2ndQuery {
  allMongodbCoursesInstructors {
    edges {
      node {
        name
        phone
        office
        email
      }
    }
  }
}
`