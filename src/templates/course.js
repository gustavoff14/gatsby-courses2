import React from "react";
import { graphql } from "gatsby";

export default function Course({ data }) {
  const course = data.mongodbCoursesCourses;
  return (
    <div>
      <h1>{course.subject} {course.course} {course.title}</h1>
      <table>
        <tr>
            <th>Subject</th>
            <td>{course.subject}</td>
        </tr>
        <tr>
            <th>Course</th>
            <td>{course.course}</td>
        </tr>
        <tr>
            <th>Title</th>
            <td>{course.title}</td>
        </tr>
        <tr>
            <th>Credits</th>
            <td>{course.credits}</td>
        </tr>
        <tr>
            <th>Description</th>
            <td>{course.description}</td>
        </tr>
    </table>
    </div>
  );
}

export const query = graphql`
  query($slug: String!) {
    mongodbCoursesCourses(fields: { slug: { eq: $slug } }) {
      subject
      course
      title
      credits
      description
    }
  }
`